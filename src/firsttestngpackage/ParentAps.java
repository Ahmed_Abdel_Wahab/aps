package firsttestngpackage;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
public class ParentAps {

    WebDriver driver;
    //driver = new ChromeDriver();
    //driver = new FirefoxDrive();
    //driver = new HtmlUnitDriver();



    @BeforeTest

    public void setup() throws InterruptedException{

	    System.setProperty("webdriver.chrome.driver","Resources/chromedriver.exe");
	    ChromeOptions options = new ChromeOptions();
	    options.setHeadless(false);
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
        driver.get("http://portal-staging.parent.eu/login");
	    driver.navigate().refresh();
	    Thread.sleep(400);
	    driver.findElement(By.id("txtEmail")).sendKeys("demo@parent.eu");
	    driver.findElement(By.id("txtPassword")).sendKeys("12345678");
        driver.findElement(By.cssSelector("#auth-page > div > form > div > button")).click();
    }
	 @Test

	public void ParentAps_Assessment() throws InterruptedException {

		 
		    
	        JavascriptExecutor js = (JavascriptExecutor) driver;
		 String KidsPalace="/html/body/app-root/app-main-layout/main/app-institution/div/div[2]/div[2]/div/div[1]/div[1]/div/div/div[1]";
			String Calendar = "body > app-root > app-header > header > app-header-topbar > nav > div "
					+ "> app-header-navbar > div > nav > ul > li:nth-child(2) > a > i";
			String Event = "body > app-root > app-main-layout > main > app-view-calendar > div >"
					+ " header > div > div > div.create__button > button.btn.btn-primary.create-event > span:nth-child(2)";
	        Thread.sleep(10000);
	        driver.findElement(By.xpath(KidsPalace)).click();
	        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	        driver.findElement(By.cssSelector(Calendar)).click();
	        driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	        Thread.sleep(3000);

	        driver.findElement(By.cssSelector(Event)).click();
	        Thread.sleep(3000);

	        driver.findElement(By.id("eventTitle")).sendKeys("TestData");
	        driver.findElement(By.id("eventDescription")).sendKeys("eventDescription");
	        Thread.sleep(300);

	        driver.findElement(By.xpath("/html/body/app-root/app-main-layout/main/app-view-calendar/app-add-event/prt-modal/div/div/div/form/perfect-scrollbar/div/div[1]/div/div[3]/prt-drop-down-list/div/div/ng-select/div/div/div[2]/input")).click();
	        Thread.sleep(400);
	        
	        driver.findElement(By.xpath("/html/body/app-root/app-main-layout/main/app-view-calendar/app-add-event/prt-modal/div/div/div/form/perfect-scrollbar/div/div[1]/div/div[3]/prt-drop-down-list/div/div/ng-select/ng-dropdown-panel/div/div[2]/div[1]/div/div/span")).click();
	        Thread.sleep(1500);
	        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/app-root/app-main-layout/main/app-view-calendar/app-add-event/prt-modal/div/div/div/form/perfect-scrollbar/div/div[1]/div/div[4]/div/div[1]/prt-date-picker/div/div/input")));
	        Thread.sleep(600);
	        driver.findElement(By.xpath("/html/body/app-root/app-main-layout/main/app-view-calendar/app-add-event/prt-modal/div/div/div/form/perfect-scrollbar/div/div[1]/div/div[4]/div/div[1]/prt-date-picker/div")).click();
	        driver.findElement(By.xpath("/html/body/bs-datepicker-container/div/div/div/div/bs-days-calendar-view/bs-calendar-layout/div[2]/table/tbody/tr[5]/td[4]/span")).click();
	        Thread.sleep(600);

	        driver.findElement(By.xpath("/html/body/app-root/app-main-layout/main/app-view-calendar/app-add-event/prt-modal/div/div/div/form/perfect-scrollbar/div/div[1]/div/div[4]/div/div[2]/prt-time-picker/div/button")).click();
	        driver.findElement(By.xpath("/html/body/bs-dropdown-container/div/ul/timepicker/table/tbody/tr[2]/td[5]/button")).click();
	        Thread.sleep(600);

	        js.executeScript("window.scrollBy(0,3500)", "");
	       driver.findElement(By.xpath("/html/body/app-root/app-main-layout/main/app-view-calendar/app-add-event/prt-modal/div/div/div/form/perfect-scrollbar/div/div[1]/div/div[4]/div/div[3]/prt-time-picker/div/button")).click();

	        Thread.sleep(300);

	       driver.findElement(By.xpath("/html/body/bs-dropdown-container/div/ul/timepicker/table/tbody/tr[2]/td[5]/button")).click();
	        Thread.sleep(300);

	        driver.findElement(By.xpath(" /html/body/bs-dropdown-container/div/ul/timepicker/table/tbody/tr[1]/td[1]/a/span")).click();
	        Thread.sleep(300);
	        
	        driver.findElement(By.xpath("/html/body/app-root/app-main-layout/main/app-view-calendar/app-add-event/prt-modal/div/div/div/form/perfect-scrollbar/div/div[1]/div")).click();


	        driver.findElement(By.cssSelector("body > app-root > app-main-layout > main > app-view-calendar > app-add-event > prt-modal > div > div > div > form > div > button")).click();
	        Thread.sleep(300);

	}
	
	@AfterMethod 
	 public void takeScreenShotOnFailure(ITestResult testResult) throws IOException { 
	 	if (testResult.getStatus() == ITestResult.FAILURE) { 
	 		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
	 		FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-" 
	 				+ Arrays.toString(testResult.getParameters()) +  ".jpg"));
	 	} 
	 }
	
   @AfterTest
   
   public void End(){

	    driver.close();
   }
}